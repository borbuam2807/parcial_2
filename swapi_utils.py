def buscar_datos_pais(root, pais):
    datos_pais = []
    for record in root.findall(".//record"):
        country = record.find(".//field[@name='Country or Area']").text
        item = record.find(".//field[@name='Item']").text
        year = record.find(".//field[@name='Year']").text
        value = record.find(".//field[@name='Value']").text

        if country.lower() == pais.lower() and item == 'Immunization, measles (% of children ages 12-23 months)':
            datos_pais.append({
                "Year": year,
                "Value": value
            })

    return datos_pais
