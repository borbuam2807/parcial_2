from flask import Flask, jsonify
import xml.etree.ElementTree as ET
from swapi_utils import buscar_datos_pais

app = Flask(__name__)

tree = ET.parse('API_SH.IMM.MEAS_DS2_en_xml_v2_5742053.xml')
root = tree.getroot()

@app.route('/vacunacion/<pais>', methods=['GET'])
def obtener_datos_vacunacion(pais):
    datos_pais = buscar_datos_pais(root, pais)
    if datos_pais:
        return jsonify(datos_pais)
    else:
        return jsonify({"message": "País no encontrado"}), 404

if __name__ == '__main__':
    app.run(debug=True)
